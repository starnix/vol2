FROM alpine:latest
MAINTAINER "David Rose david.rose@starnix.se"

RUN apk add --no-cache bison capstone flex file git jansson openssl python2 su-exec tini bash && \
    apk add --no-cache -t .build-deps py2-setuptools autoconf automake build-base cargo file-dev g++ gcc jansson-dev jpeg-dev libc-dev libffi-dev libpng-dev libressl-dev libtool libusb-dev make musl-dev openssl-dev python2-dev rust snappy-dev zlib-dev && \
    python -m ensurepip --upgrade && \
    pip2 install --upgrade pip && \
    pip2 install setuptools && \
    pip2 install Pillow PyCrypto distorm3 ipython openpyxl pytz ujson && \
    git clone --recursive https://github.com/VirusTotal/yara.git /tmp/yara && \
    (cd /tmp/yara; ./bootstrap.sh && sync && ./configure --with-crypto --enable-magic --enable-cuckoo && make && make install) && \
    git clone --recursive https://github.com/VirusTotal/yara-python /tmp/yara-python && \
    (cd /tmp/yara-python/; python2 setup.py build --dynamic-linking && python2 setup.py install) && \
    rm -Rf /tmp/yara && \
    rm -Rf /tmp/yara-python && \
    apk del --purge .build-deps && \
    git clone https://github.com/volatilityfoundation/volatility.git /vol

ADD entrypoint.sh /entrypoint.sh

VOLUME /files

WORKDIR /files

ENTRYPOINT [ "/entrypoint.sh" ]

