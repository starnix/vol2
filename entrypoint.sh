#!/bin/bash

PROFILE_USAGE="\n\nTo add a custom profile, create a directory of either 'linux', 'mac' or 'windows' in the current directory.  Zip files within the directory will automatically be added to the overlays directory\n"

if [ "${#}" -lt 1 ]; then
    /usr/bin/python /vol/vol.py --help
    echo -e $PROFILE_USAGE
    exit 0
fi

for os in linux mac windows; do
    if [ -d /files/${os} ]; then
	cp /files/${os}/*.zip /vol/volatility/plugins/overlays/${os}/
    fi;
done

/usr/bin/python /vol/vol.py "${@}"
